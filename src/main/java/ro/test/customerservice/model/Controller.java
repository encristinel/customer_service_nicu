package ro.test.customerservice.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.test.customerservice.exceptions.CustomerNotFoundEx;


@RestController
public class Controller {

    @Autowired
    private CustomerService customerService;

    public Controller(CustomerService customerService) {
        this.customerService = customerService;
    }

    //listare clienti
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public List<Customers> get() {
        return customerService.getAll();
    }


    //actualizare numar telefon pe baza MSISDN
    @RequestMapping(value = "/customers/{ocn}/msisdn", method = RequestMethod.PUT)
    public Customers updateCustomers(@PathVariable("ocn") String ocn, @RequestBody String msisdn) throws CustomerNotFoundEx {
    return customerService.updateCustomerContactPhone(customerService.getCustomerByOcn(ocn),msisdn);

    }

    //return abonat pe baza codului (try catch)
    @RequestMapping(value = "/customers/{ocn}", method = RequestMethod.GET)
    public Customers getCustomers(@PathVariable("ocn") String ocn) throws CustomerNotFoundEx{

        return customerService.getCustomerByOcn(ocn);
    }

}
