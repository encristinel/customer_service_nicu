package ro.test.customerservice.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.test.customerservice.exceptions.CustomerNotFoundEx;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(CustomerNotFoundEx.class)
    @ResponseBody
    @JsonView(ExceptionRestView.class)
    public CustomerNotFoundEx handleCustomerNotFound(CustomerNotFoundEx exceptie, HttpServletResponse response) {
        response.setStatus(HttpStatus.NOT_FOUND.value());
        return exceptie;
    }


}
