package ro.test.customerservice.model;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.test.customerservice.exceptions.CustomerNotFoundEx;

import java.util.List;

@Service

public class CustomerService {

    public Customers getCustomerByOcn(String ocn) throws CustomerNotFoundEx {
        Assert.notNull(ocn, "Codul de abonat nu poate fi null");

        for (Customers cust : PseudoCustomers.getList()) {

            if (ocn.equals(cust.getOcn())) {

                return cust;
            }

        }

        throw new CustomerNotFoundEx("nu exista codul", 123);

    }

    public List<Customers> getAll() {
        System.out.println("Clienti " + PseudoCustomers.getList());
        return PseudoCustomers.getList();
    }

    public Customers updateCustomerContactPhone(Customers custdeact, String nrtelefon) throws CustomerNotFoundEx {
        custdeact.setMsisdn(nrtelefon);

        return getCustomerByOcn(custdeact.getOcn());
    }
}