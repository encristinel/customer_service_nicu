package ro.test.customerservice.model;

public class Customers {

    private String name;
    private String ocn;
    private String address;
    private Integer billday;
    private String msisdn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOcn() {
        return ocn;
    }

    public void setOcn(String ocn) {
        this.ocn = ocn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getBillday() {
        return billday;
    }

    public void setBillday(Integer billday) {
        this.billday = billday;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public Customers (){

    }
    public Customers(String name, String ocn, String address, Integer billday, String msisdn) {

        this.name = name;
        this.ocn = ocn;
        this.address = address;
        this.billday = billday;
        this.msisdn = msisdn;
    }


}

