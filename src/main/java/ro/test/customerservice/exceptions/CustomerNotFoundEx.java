package ro.test.customerservice.exceptions;

import com.fasterxml.jackson.annotation.JsonView;
import ro.test.customerservice.model.ExceptionRestView;

public class CustomerNotFoundEx extends Exception {
    private Integer id;

    public CustomerNotFoundEx(String message, Integer id) {
        super(message);
        this.id = id;
    }
@JsonView(ExceptionRestView.class)
    public Integer getId() {
        return id;
    }
@JsonView (ExceptionRestView.class)
    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
